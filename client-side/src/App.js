import "./App.css";
import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MyUser from "./pages/user/user";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<MyUser />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
